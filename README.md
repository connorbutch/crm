# crm
This project shows using multiple languages within the same aws sam application.

There is a [node js lambda backing an api gateway](https://gitlab.com/connorbutch/crm/-/tree/main/create-contact).  It validates input, then places valid requests on sqs.  

There is a [java lambda that polls the sqs](https://gitlab.com/connorbutch/crm/-/tree/main/save-contact-to-db).  It saves input to dynamodb.

Placing items on a queue before saving to database can be useful.  Some reasons to do this include:
- shielding client from failures writing to the database
- automatic retries with exponential backoff (more than the dynamodb client already has)
- ability to use dead letter queues for reprocessing
- buffering before saving if you use a relational database, which has a limited number of connections and can get overwhelmed by lambda (without rds proxy)
- cost reasons -- if you want to provision throughput on dynamodb, you can use reserved concurrency on lambda to not exceed the provisioned write throughput