import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.FailedBatch;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutRequest;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSBatchResponse;
import com.amazonaws.services.lambda.runtime.events.SQSBatchResponse.BatchItemFailure;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class RequestHandlerSaveContactImpl implements RequestHandler<SQSEvent, SQSBatchResponse> {
    private static final Gson GSON = new Gson();

    private final DynamoDBMapper dynamoDBMapper;

    public RequestHandlerSaveContactImpl(DynamoDBMapper dynamoDBMapper) {
        this.dynamoDBMapper = dynamoDBMapper;
    }

    public RequestHandlerSaveContactImpl() {
        this(new DynamoDBMapper(AmazonDynamoDBClientBuilder.defaultClient()));
    }

    @Override
    public SQSBatchResponse handleRequest(SQSEvent input, Context context) {
        Map<String, String> contactIdToSqsMessageId = new HashMap<>();
        Object[] contactsToSaveToDb = input.getRecords().stream()
                .map(message -> this.deserializeMessageAndSaveIdMapping(message, contactIdToSqsMessageId))
                .toArray();

        //it's usually best to batch calls in aws, as you are charged per request
        List<FailedBatch> failedBatches = dynamoDBMapper.batchSave(contactsToSaveToDb);

        List<BatchItemFailure> batchItemFailures = failedBatches.get(0).getUnprocessedItems().get("contact-table").stream()
                .map(WriteRequest::getPutRequest)
                .filter(Objects::nonNull)
                .map(PutRequest::getItem)
                .map(map -> map.get("id"))
                .map(AttributeValue::getS)
                .map(contactIdToSqsMessageId::get)
                .map(BatchItemFailure::new)
                .collect(Collectors.toList());

        return new SQSBatchResponse(batchItemFailures);
    }

    private Contact deserializeMessageAndSaveIdMapping(SQSMessage message, Map<String, String> contactIdToSqsMessageId) {
        Contact contact = GSON.fromJson(message.getBody(), Contact.class);
        contactIdToSqsMessageId.put(contact.getId(), message.getMessageId());
        return contact;
    }
}
