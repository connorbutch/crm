const AWS = require('aws-sdk');
const sqs = new AWS.SQS({ region: process.env.REGION });
const { v4: generateUUID } = require('uuid');

let response;

exports.handler = async (event, context) => {
    const body = JSON.parse(event.body);
    if (body == null || body.firstName == null || body.lastName == null) {
        console.log("Invalid request");
        //in production, we would give an error response with a specific id and message so the user could fix their mistake (such as first name is required)...
        response = {
            'statusCode': 400
            //would have a response body here
        }

    } else {
        console.log("Valid request");
        const generatedUserId = generateUUID();
        console.log("Generated id" + generatedUserId);
        body.id = generatedUserId;
        const sqsParams = {
            MessageBody: JSON.stringify(body),
            QueueUrl: process.env.QUEUE_URL
        };
        console.log(sqsParams);
        try {
            const sendMessageResult = await sqs.sendMessage(sqsParams).promise();
            console.log("Successfully sent message to sqs " + JSON.stringify(sendMessageResult));
            response = {
                'statusCode': 202,
                'body': JSON.stringify(body)
            }
        } catch (err) {
            console.log(err);
            const responseBody = [{
                "id": 99999,
                "message": "Internal server error"
            }];
            response = {
                'statusCode': 500,
                'body': JSON.stringify(responseBody)
            }
        }
    }
    return response;
};